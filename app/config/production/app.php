<?php

return array(

    'debug' => false,
    'manga_path' => '/home/manga/public',
    'images_path' => '/home/manga/images',
    'url' => 'https://manga.madokami.com',
    'require_auth' => true,

);
